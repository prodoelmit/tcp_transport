#pragma once
#include <fstream>

// Singleton logger
class Logger
{
public:
	static Logger& getInstance()
	{
		static Logger l;
		return l;
	}

	void init(const std::string filename);
	void info(const std::string& msg);
private:
	Logger() {;;};
	~Logger()
	{
		if (out.is_open())
		{
			out.close();
		}
	};

	Logger(Logger const&) = delete;
	Logger& operator= (Logger const&) = delete;

private:
	std::ofstream out;
};

#define qInfo Logger::getInstance().info
