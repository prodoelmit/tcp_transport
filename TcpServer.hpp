#pragma once
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <string>
#include "Broker.hpp"
#include "Common.hpp"
#include "Enums.hpp"


// handles one connection per run
// didn't have time to make it real multi-socket server
class TcpServer {
	public:
		TcpServer();
		~TcpServer();
		bool listen(int port = 1337);
		void run();
		Enums::ErrorCode lastErrorCode() const;
		std::string lastErrorString() const;

	private:
		void setError(Enums::ErrorCode code, std::string&& errorString);
		int _tcpBinding = 0;
		int _tcpSocket = 0;
		size_t _currentOffset = 0;
		char* _buffer = nullptr;
		Enums::ErrorCode _lastErrorCode = Enums::ErrorCode::NoError;
		std::string _lastErrorString;
		std::shared_ptr<Broker> _broker;
		struct sockaddr_in _serverAddress, _clientAddress;

};
