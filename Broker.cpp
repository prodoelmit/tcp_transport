#include "Broker.hpp"
#include <cstring>
#include <iostream>

Broker::Broker(int transactionSegmentsCount) :
		_segmentsCount(transactionSegmentsCount)
{
	for (int i = 0; i < transactionSegmentsCount; i++)
	{
		_processors.emplace_back(std::make_shared<SegmentProcessor>(i));
	}
}

void Broker::processTransaction(char* transaction)
{
	// using strtok here because of it's zero-copying behaviour
	// which is faster
	// same applies to using char*
	std::cout << "parseTransaction " << std::string(transaction) << std::endl;
	char* token = nullptr;
	char delim = '|';
	char endchar = '\0';
	for (int i = 0; i < _segmentsCount; i++)
	{
		if (i == 0)
		{
			token = strtok(transaction, &delim);
			if (token == nullptr) { break; }
		}
		else if (i != _segmentsCount - 1)
		{
			token = strtok(nullptr, &delim);
			if (token == nullptr) { break; }
		}
		else
		{
			// send everything that's left to last processor
			token = strtok(nullptr, &endchar);
			if (token == nullptr) { break; }
		}

		// this might be made multithreaded,
		// but aware that `transaction` will be
		// freed soon
		_processors[i]->processSegment(token);
	}
}
