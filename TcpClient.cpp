#include "TcpClient.hpp"
#include <netdb.h>
#include <strings.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstring>
#include <cassert>
#include "Common.hpp"
#include "Logger.hpp"

TcpClient::TcpClient()
{
}

TcpClient::~TcpClient()
{
	if (_tcpSocket)
	{
		close(_tcpSocket);
	}
}

bool TcpClient::connect(const std::string& hostname, int port)
{
	if (_tcpSocket)
	{
		setError(Enums::ErrorCode::AlreadyConnected, "in connect(): trying to connect while another connection is open");
		return false;
	}
	struct sockaddr_in serverAddress;
	struct hostent* server;

	// open socket
	_tcpSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (_tcpSocket < 0)
	{
		setError(Enums::ErrorCode::FailedOpeningSocket, "in connect(): Failed opening socket");
		_tcpSocket = 0;
		return false;
	}

	// prepare destination params
	server = gethostbyname(hostname.data());
	if (server == NULL)
	{
		setError(Enums::ErrorCode::CouldntConnect, "in connect(): No such host");
		close(_tcpSocket);
		return false;
	}
	bzero((char*)&serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	bcopy((char*)server->h_addr, (char*)&serverAddress.sin_addr.s_addr, server->h_length);
	serverAddress.sin_port = htons(port);

	// connect to server
	if (::connect(_tcpSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0)
	{
		setError(Enums::ErrorCode::CouldntConnect, "in connect(): Failed connecting");
		close(_tcpSocket);
		return false;
	}

	return true;
}

void TcpClient::setError(Enums::ErrorCode code, std::string&& errorString)
{
	_lastErrorCode = code;
	_lastErrorString = errorString;

	// log
	qInfo(_lastErrorString);
}

void TcpClient::disconnect()
{
	if (_tcpSocket)
	{
		close(_tcpSocket);
	}
}

// test function
void TcpClient::sendString(std::string msg)
{
	if (_tcpSocket == 0)
	{
		return;
	}
	const char* data = msg.data();
	write(_tcpSocket, data, sizeof(data));
}

std::string TcpClient::lastErrorString() const
{
	return _lastErrorString;
}

bool TcpClient::enqueueTransaction(const std::string& transaction)
{
	const char* data = transaction.data();
	size_t len = transaction.size();

	// check that there is enough space in buffer for this transaction
	if (_currentOffset + len + 1 >= CLIENT_BUFFER_SIZE)
	{
		setError(Enums::ErrorCode::BufferOverflow, "in enqueueTransaction(): Buffer doesn't have enough space left");
		return false;
	}

	bcopy(data, _buffer + _currentOffset, len);
	_currentOffset += len;
	_buffer[_currentOffset] = TRANSACTION_DELIMITER;
	_currentOffset += 1;

	return true;
}

bool TcpClient::send()
{
	if (_tcpSocket == 0)
	{
		setError(Enums::ErrorCode::NotConnected, "in send(): socket isn't opened");
		return false;
	}

	const char* data = _buffer;
	write(_tcpSocket, data, _currentOffset);
	clearQueue();
	return true;
}

void TcpClient::clearQueue()
{
	bzero(_buffer, CLIENT_BUFFER_SIZE);
	_currentOffset = 0;
}

Enums::ErrorCode TcpClient::lastErrorCode() const
{
	return _lastErrorCode;
}
