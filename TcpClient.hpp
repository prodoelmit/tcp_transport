#pragma once
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <vector>
#include <string>
#include "Enums.hpp"

#define CLIENT_BUFFER_SIZE 1024

class TcpClient 
{
	public:
		TcpClient();
		~TcpClient();
		bool connect(const std::string& hostname = "127.0.0.1", int port = 1337);
		void disconnect();
		void sendString(std::string msg);
		bool enqueueTransaction(const std::string& transaction);
		void clearQueue();
		bool send();
		Enums::ErrorCode lastErrorCode() const;
		std::string lastErrorString() const;

	private:

		void setError(Enums::ErrorCode code, std::string&& errorString);

		char _buffer[CLIENT_BUFFER_SIZE] {0};
		size_t _currentOffset = 0;
		int _tcpSocket = 0;

		Enums::ErrorCode _lastErrorCode = Enums::ErrorCode::NoError;
		std::string _lastErrorString;


};
