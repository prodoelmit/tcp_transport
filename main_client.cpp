#include "TcpClient.hpp"
#include <string>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <unistd.h>
#include "Logger.hpp"

int main(int argc, char** argv)
{
	Logger::getInstance().init("/tmp/tcp_client.log");
	TcpClient tcp = TcpClient();
	{
		bool ok = tcp.connect();
		if (!ok)
		{
			std::cout << "Error connecting: " << tcp.lastErrorString() << std::endl;
		}
	}

	std::vector<std::string> transactions;
	for (int i = 0; i < 100; i++)
	{
		std::ostringstream os;
		os << i << "|" << rand() << "|" << rand();
		transactions.emplace_back(os.str());
	}

	int per_packet = 25;
	int packets = 2;
	for (int i = 0; i < packets; i++)
	{
		for (int j = per_packet * i; j < per_packet*i + per_packet; j++)
		{
			std::cout << "Enqueueing transaction " << transactions.at(j) << std::endl;
			bool ok = tcp.enqueueTransaction(transactions.at(j));
			if (!ok)
			{
				std::cout << "Error enqueueing: " << tcp.lastErrorString() << std::endl;
			}
		}
		bool ok = tcp.send();
		if (!ok)
		{
			std::cout << "Error sending: " << tcp.lastErrorString() << std::endl;
		}
	}

	return 0;
}
