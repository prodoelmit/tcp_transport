#pragma once
#include <memory>
#include "SegmentProcessor.hpp"
#include <vector>

class Broker
{
	public:
		Broker(int transactionSegmentsCount);
		void processTransaction(char* transaction);

	private:

		size_t _segmentsCount;
		std::vector<std::shared_ptr<SegmentProcessor>> _processors;
};
