class Enums
{
public:
	enum class ErrorCode: uint16_t
	{
		NoError = 0,
		NotConnected = 1,
		ConnectionClosed = 2,
		AlreadyConnected = 3,
		FailedOpeningSocket = 4,
		CouldntConnect = 5,
		BufferOverflow = 6,
		AlreadyListening = 7,
		BindingError = 8,
		AcceptError = 9

	};
};
