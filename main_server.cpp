#include "TcpServer.hpp"
#include <string>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include "Logger.hpp"


int main(int argc, char** argv)
{
	Logger::getInstance().init("/tmp/tcp_server.log");
	TcpServer tcp = TcpServer();
	for (int i = 1; i < 5; i++) {
	bool ok = tcp.listen(1337);
	if (!ok)
	{
		std::cout << "Error listening: " << tcp.lastErrorString() << std::endl;
	}
	}

	std::cout << "Stopping..." << std::endl;

	return 0;
}
