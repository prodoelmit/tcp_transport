#include "TcpServer.hpp"
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <cassert>
#include "Logger.hpp"

TcpServer::TcpServer()
{
	_broker = std::make_shared<Broker>(5);
}

TcpServer::~TcpServer()
{
	// need to properly unlink socket
	if (_tcpBinding > 0)
	{
		close(_tcpBinding);
	}
}

bool TcpServer::listen(int port)
{
	// handles one socket connection
	if (_tcpBinding)
	{
		setError(Enums::ErrorCode::AlreadyListening, "in listen(): trying to start listening while another socket is open");
		return false;
	}

	int clientLength;
	struct hostent* server;

	// open socket
	_tcpBinding = socket(AF_INET, SOCK_STREAM, 0);
	if (_tcpBinding < 0)
	{
		setError(Enums::ErrorCode::FailedOpeningSocket, "in listen(): Failed opening socket");
		_tcpBinding = 0;
		return false;
	}

	bzero((char*)&_serverAddress, sizeof(_serverAddress));
	_serverAddress.sin_family = AF_INET;
	_serverAddress.sin_addr.s_addr = INADDR_ANY;
	_serverAddress.sin_port = htons(port);

	if (bind(_tcpBinding, (struct sockaddr*)&_serverAddress, sizeof(_serverAddress)) < 0)
	{
		setError(Enums::ErrorCode::BindingError, "in listen(): Failed to bind");
		return false;
	}

	::listen(_tcpBinding, 5);

	clientLength = sizeof(_clientAddress);
	_tcpSocket = accept(_tcpBinding, (struct sockaddr*)&_clientAddress, (socklen_t*)&clientLength);

	if (_tcpSocket < 0)
	{
		setError(Enums::ErrorCode::AcceptError, "in listen(): couldn't accept binding");
		close(_tcpBinding);
		return false;
	}

	run();
}

void TcpServer::run()
{
	_buffer = static_cast<char*>(malloc(MAX_MESSAGE_SIZE));
	int n = 0;
	while (true)
	{
		n = read(_tcpSocket, _buffer, MAX_MESSAGE_SIZE - 1);
		if (n > 0)
		{
			std::cout << "read something, n = " << n << std::endl ;
			bool first = true;
			char* token = nullptr;
			char delim = TRANSACTION_DELIMITER;
			char* saveptr;
			while (true)
			{
				if (first)
				{
					first = false;
					token = strtok_r(_buffer, &delim, &saveptr);
				}
				else
				{
					token = strtok_r(nullptr, &delim, &saveptr);
				}
				if (token != nullptr)
				{
					_broker->processTransaction(token);
				}
				else
				{
					break;
				}
			}
			bzero(_buffer, MAX_MESSAGE_SIZE);
		}
		else if (n == -1)
		{
			std::cout << "Error reading socket, exiting" << std::endl;
			break;
		}
	}
	/* delete[] _buffer; */
}

std::string TcpServer::lastErrorString() const
{
	return _lastErrorString;
}

Enums::ErrorCode TcpServer::lastErrorCode() const
{
	return _lastErrorCode;
}

void TcpServer::setError(Enums::ErrorCode code, std::string&& errorString)
{
	_lastErrorCode = code;
	_lastErrorString = errorString;

	// log
	qInfo(_lastErrorString);
}
