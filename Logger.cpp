#include "Logger.hpp"

void Logger::init(std::string filename)  {
	if (out.is_open()) {
		out.close();
	}
	out = std::ofstream(filename);
}

void Logger::info(const std::string& msg)
{
	if (out.is_open()) {
		out << msg << std::endl;
	}
}
