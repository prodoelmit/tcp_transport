#include "SegmentProcessor.hpp"
#include <iostream>

SegmentProcessor::SegmentProcessor(int id):
	_id(id)
{
}

void SegmentProcessor::processSegment(const char* segment) 
{
	std::cout << "Processor " << _id << ": " << std::string(segment) << std::endl;
}
